FROM node:21-alpine

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000
