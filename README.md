# LANDING PAGE AROSAJE

## INSTALLATION

Faire les commandes ci-dessous pour installer le projet : <br>
Remarques importantes : Installer le projet dans votre WSL si vous êtes sur windows !

```sh
#clonage du projet
git clone

cd site-vitrine

# Création du container docker
docker compose up -d --build

# Copie des node_modules et .nuxt
docker cp arosaje_site_vitrine:/app/node_modeules ./

docker cp arosaje_site_vitrine:/app/.nuxt ./
```

## LANCEMENT PROJET

```sh
docker compose up
```

Retrouvez le projet ici : http://localhost:3000