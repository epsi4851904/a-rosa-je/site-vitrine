import { defineStore } from 'pinia';

export const useDarkModeStore = defineStore('darkMode', {
    state: () => ({
        isDarkMode: false,
    }),
    getters: {
        getIsDarkMode: (state) => state.isDarkMode,
    },
    actions: {
        toggleDarkMode() {
            this.isDarkMode = !this.isDarkMode;
            if(this.isDarkMode){
                document.documentElement.classList.add('dark')
            }else {
                document.documentElement.classList.remove('dark')
            }
        },
    },
});